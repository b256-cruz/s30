// count the total number of fruits on sale
db.fruits.aggregate([
    { $match: {onSale: true}},
    {$count: "Fruits on sale"},
])

// count the total number of fruits with stock more than or equal to 20
db.fruits.aggregate([
    { $match: {stock: { $gte: 20 }} },
    { $count: "Enough Stock" }
])
   
// Get the average price of fruits onSale per supplier
db.fruits.aggregate([
    { $match: {onSale: true}},
    {$group: {_id: "$supplier_id", avgStocks: {$avg: "$price"}}}
])

// Get the highest price of a fruit per supplier
db.fruits.aggregate([
    { $match: {onSale: true}},
    {$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}},
])

// Get the lowest price of a fruit per supplier
db.fruits.aggregate([
    { $match: {onSale: true}},
    {$group: {_id: "$supplier_id", lowPrice: {$min: "$price"}}},
]) 